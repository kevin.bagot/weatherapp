import { defineStore } from "pinia";
import axios from "axios";

export const useStore = defineStore({
  id: "store",
  state: () => ({
    cityZip: null,
    cities: {},
    weatherData: {},
    conditions: null,
    dateTime: null,
    recentSearches: [],
    favorite: null,
    error: null,
  }),
  getters: {
    // arrondie les temperatures
    roundedValues: (state) => {
      const roundData = {};
      Object.keys(state.weatherData).map((key) => {
        return (roundData[key] = Math.round(state.weatherData[key]));
      });
      return roundData;
    },
  },
  actions: {
    // fonction call api weather en fonction du code postal rechercher et stock dans le store
    async searchWeather(zipValue) {
      try {
        const res = await axios.get(
          `https://api.openweathermap.org/data/2.5/weather?zip=${zipValue},FR&appid=c2bde5cd6b27d878a17240331d47eae8&units=metric&lang=fr`
        );
        this.cityZip = zipValue;
        this.cities = res.data.name;
        this.weatherData = res.data.main;
        this.dateTime = new Date(res.data.dt * 1000).toLocaleString("fr-FR");
        this.favorite = null;

        const condition = res.data.weather.map((elm) => {
          return {
            description: elm.description,
            main: elm.main,
          };
        });

        for (const value of condition.values()) {
          this.conditions = value;
        }
      } catch (error) {
        this.error = error;
      }
    },
    // ajoute la derniere recherche au favoris
    addSearchToFavorite() {
      const allSearches = [];
      allSearches.push({
        name: this.cities,
        zip: this.cityZip,
        data: this.roundedValues,
      });
      allSearches.forEach((item) => {
        if (!this.recentSearches.find((e) => e.zip === item.zip)) {
          this.recentSearches.unshift(item);

          this.favorite = true;

          if (this.recentSearches.length > 5) {
            this.recentSearches.pop();
          }
        }
      });
    },
  },
});
