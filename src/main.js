import { createPinia } from "pinia";
import { createApp, watch } from "vue";
import App from "./App.vue";
import "./index.css";
import router from "./router";

const pinia = createPinia();
//recharge pinia state depuis le localStorage
if (localStorage.getItem("state")) {
  pinia.state.value = JSON.parse(localStorage.getItem("state"));
}

//save pinia state dans le local storage
watch(
  pinia.state,
  (state) => {
    localStorage.setItem("state", JSON.stringify(state));
  },
  { deep: true }
);

createApp(App).use(router).use(pinia).mount("#app");
